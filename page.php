<style>
	.woocommerce-placeholder {display:none!important;}
	.woocommerce ul.products li.product h3 {
    padding: .5em 0;
    margin: 0;
   font-size: 24px!important;
}
	.woocommerce ul.products li.product, .woocommerce-page ul.products li.product {
    float: left;
    margin: 0 3.8% 2.992em 0;
    padding: 0;
    position: relative;
    width: 100%!important;
}
	.woocommerce ul.products li.product .price {
    color: #77a464;
    display: block;
    font-weight: 400;
    margin-bottom: .5em;
    font-size: 18px!important;
}
	.woocommerce ul.products li.product h3 {
    display:none!important;
}



.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
    font-size: 100%;
    margin: 0;
    line-height: 1;
    cursor: pointer;
    position: relative;
    text-decoration: none;
    overflow: visible;
    padding: .618em 1em;
    font-weight: 700;
    border-radius: 3px;
    left: auto;
    color: #fff!important;
    background-color: #D55014!important;
    border: 0;
    white-space: nowrap;
    display: inline-block;
    background-image: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    text-shadow: none;
	}

	.woocommerce ul.products li.product .price {
    color: #1e6c9b!important;
    display: block;
    font-weight: 600!important;
    margin-bottom: .5em;
    font-size: 18px;
}

.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
    background-color: #eb0520!important;
    color: #fff;
    -webkit-font-smoothing: antialiased;
}

.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover {
  background-color: #eb0520!important;
    color: #fff;
}
</style>
<?php get_header(); ?>
 <?php if ( is_front_page() ) : ?>
 <?php get_template_part('inc/template','homecols'); ?>
<?php else : ?>
<?php get_template_part('inc/strip'); ?>
<?php endif; ?>
				<div class="container-fluid" style="margin-top:38px;">
<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-9">
	<main role="main">
		<!-- section -->
		<section>

			<!--<h1><?php //the_title(); ?></h1>-->

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php
					   if (is_front_page()){
						   get_template_part('inc/template','home');
					   } else if (is_page('fitness-videos')) {
						   get_template_part('inc/template','video');
					   } else if (is_page('fitness-articles')){
						    get_template_part('inc/template','blog');
					   } else if (is_page('fitness-products')) {
							 	get_template_part('inc/template','products');
						 } else {
					   the_content();
					   }
				?>


				<?php //comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
<?php get_sidebar();

?>
	</div>
</div>
<?php get_template_part('inc/forms'); ?>
<?php get_footer(); ?>
