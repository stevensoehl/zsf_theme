<div class="row">

  <div class="col-xs-12">

    <h2 class="home-title">Fitness Products</h2>

  </div>

</div>

<div class="woocommerce columns-4">

          <div class="products">

<?php
$args = array( 'post_type' => 'product', 'posts_per_page' => 4,  'product_tag' => 'homepage' );

                 $loop = new WP_Query( $args );

                 while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


   <div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

                             <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

                                 <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                                 <div class="top-grid-img">

                                   <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

                                 </div>
                               </a>
                                 <div class="" style="margin-top:15px; text-align:center;">

                                   <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

                                   <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
                 <div style="margin-top:10px; padding-bottom:10px;">
                   <a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
                     onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Products - <?php the_title(); ?>');">BUY NOW</a>
                 </div>

                                 </div>



                         </div>

<?php
 endwhile; ?>

             <?php wp_reset_query(); ?>
</div>
</div>

<div style="position:relative; float:right; padding-right:10px; margin-top:25px; font-size:17px;">
  <a href="<?php echo home_url(); ?>/fitness-products/" style="color:#901b1e;" onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Products - More');">More Supplements</a></div>
<!-- end products -->
