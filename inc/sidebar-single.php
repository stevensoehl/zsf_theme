<?php
$category = get_the_category();
if(!empty($category)) {
$firstCategory = $category[0]->slug;
$catName = $category[0]->cat_name;
}

if ($firstCategory == 'fitness-articles') {

  $args = array(
  'category__and' => 1,
  'posts_per_page' => 5
  );

} else if ($firstCategory == 'fitness-videos') {
  $args = array(
  'category__and' => 1902,
  'posts_per_page' => 5
  );

}
 ?>
<div class="sidebar-widget">
    <div class="widget">
      <div class="title">More <?php echo $catName; ?></div>
 <?php
 $posts = get_posts($args);
         foreach ($posts as $post) : ?>
           <div style="padding:5px 0;font-size:15px;"><a style="color:#aa0606;" href="<?php echo get_permalink( $post->ID ); ?>" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'Sidebar-Posts', 'event_action': 'click', 'event_label': '<?php echo $post->post_title; ?>'});"><?php echo $post->post_title; ?></a></div>
 <?php    endforeach;   ?>

</div>
</div>
