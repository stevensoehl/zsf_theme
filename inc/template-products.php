<?php the_content(); ?>

 <h2 style="font-family: 'Oswald', sans-serif; color:#000; text-align:center;">PRE WORKOUTS</h2>
 <div class="woocommerce columns-4">

           <div class="products">

<?php
 $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'pre-workouts', 'product_tag' => 'featured' );

                  $loop = new WP_Query( $args );

                  while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


	  <div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

                              <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                                title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>
                                onClick="ga('send', 'event', 'Product Page', 'Click', 'Pre-Workouts Image - <?php the_title(); ?>');">

                                  <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                                  <div class="top-grid-img">

                                    <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

                                  </div>
                                </a>
                                  <div class="" style="margin-top:15px; text-align:center;">

                                    <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

                                    <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
									<div style="margin-top:10px; padding-bottom:10px;">
										<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
                      onClick="ga('send', 'event', 'Product Page', 'Click', 'Pre-Workouts - <?php the_title(); ?>');">BUY NOW</a>
									</div>

                                  </div>



                          </div>

<?php
  endwhile; ?>

              <?php wp_reset_query(); ?>
</div>
</div>
<div style="position:relative; float:right; padding-right:10px; margin-top:25px; font-size:17px;"><a href="/product-category/health-and-personal-care/sports-nutrition/pre-workout/">More Pre Workouts</a></div>

<!-- end pre workouts -->
<hr style="margin-top:35px;">
<div style="padding-top:35px; clear:both;">

 <h2 style="font-family: 'Oswald', sans-serif; color:#000; text-align:center; margin-top:35px;">POST WORKOUT &amp; RECOVERY</h2>
 <div class="woocommerce columns-4">

           <div class="products">

<?php
 $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'post-workouts', 'product_tag' => 'featured' );

                  $loop = new WP_Query( $args );

                  while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


	  <div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

                              <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>
                                onClick="ga('send', 'event', 'Product Page', 'Click', 'Post-Workouts Image - <?php the_title(); ?>');">

                                  <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                                  <div class="top-grid-img">

                                    <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

                                  </div>
                                </a>
                                  <div class="" style="margin-top:15px; text-align:center;">

                                    <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

                                    <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
									<div style="margin-top:10px; padding-bottom:10px;">
										<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
                      onClick="ga('send', 'event', 'Product Page', 'Click', 'Post-Workouts BUY NOW - <?php the_title(); ?>');">BUY NOW</a>
									</div>

                                  </div>



                          </div>

<?php
  endwhile; ?>

              <?php wp_reset_query(); ?>
</div>
</div>
<div style="position:relative; float:right; padding-right:10px; margin-top:25px; font-size:17px;">
  <a href="/product-category/health-and-personal-care/sports-nutrition/post-workout-and-recovery/"
  onClick="ga('send', 'event', 'Product Page', 'Click', 'Post-Workouts - More');">More Post Workouts</a></div>

</div>

<!-- end post workouts -->

<hr style="margin-top:35px;">
<div style="padding-top:35px; clear:both;">

 <h2 style="font-family: 'Oswald', sans-serif; color:#000; text-align:center; margin-top:35px;">PROTEIN</h2>
 <div class="woocommerce columns-4">

           <div class="products">

<?php
 $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'protein', 'product_tag' => 'featured' );

                  $loop = new WP_Query( $args );

                  while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


	  <div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

                              <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                                title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" onClick="ga('send', 'event', 'Product Page', 'Click', 'Protein Image - <?php the_title(); ?>');">

                                  <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                                  <div class="top-grid-img">

                                    <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

                                  </div>
                                </a>
                                  <div class="" style="margin-top:15px; text-align:center;">

                                    <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

                                    <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
									<div style="margin-top:10px; padding-bottom:10px;">
										<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
                      onClick="ga('send', 'event', 'Product Page', 'Click', 'Protein BUY NOW - <?php the_title(); ?>');">BUY NOW</a>
									</div>

                                  </div>



                          </div>

<?php
  endwhile; ?>

              <?php wp_reset_query(); ?>
</div>
</div>
<div style="position:relative; float:right; padding-right:10px; margin-top:25px; font-size:17px;"><a href="/product-category/health-and-personal-care/sports-nutrition/protein/"
  onClick="ga('send', 'event', 'Product Page', 'Click', 'Protein More ');">More Protein Powders</a></div>

</div>

<!-- end protein -->
<div class="clearfix"></div>
<hr style="margin-top:35px;">
<div style="padding-top:35px; clear:both;">

 <h2 style="font-family: 'Oswald', sans-serif; color:#000; text-align:center; margin-top:35px;">Testosterone Boosters</h2>
 <div class="woocommerce columns-4">

           <div class="products">

<?php
 $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'testosterone-boosters', 'product_tag' => 'featured' );

                  $loop = new WP_Query( $args );

                  while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


	  <div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

                              <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                                title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"
                                onClick="ga('send', 'event', 'Product Page', 'Click', 'Test Booster Image - <?php the_title(); ?>');">

                                  <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                                  <div class="top-grid-img">

                                    <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

                                  </div>
                                </a>
                                  <div class="" style="margin-top:15px; text-align:center;">

                                    <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

                                    <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
									<div style="margin-top:10px; padding-bottom:10px;">
										<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
                      onClick="ga('send', 'event', 'Product Page', 'Click', 'Test Booster BUY NOW - <?php the_title(); ?>');">BUY NOW</a>
									</div>

                                  </div>



                          </div>

<?php
  endwhile; ?>

              <?php wp_reset_query(); ?>
</div>
</div>
<div style="position:relative; float:right; padding-right:10px; margin-top:25px; font-size:17px;">
  <a href="/product-category/health-and-personal-care/sports-nutrition/testosterone-boosters/"
  onClick="ga('send', 'event', 'Product Page', 'Click', 'Test Booster More');">More Test Boosters</a></div>

</div>

<!-- end test boosters -->
