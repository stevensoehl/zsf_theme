 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	jQuery(document).ready(function() {

    $('.form-div').hide();
    $('.btn-open').click(function(){
        var target = "#" + $(this).data("target");
        $(".form-div").not(target).hide();
        $(target).show(300);
    });
	
	$('.btn-close').click(function(){
        var target = "#" + $(this).data("target");
       
        $(target).hide(300);
    });

});
</script>


<div class="form-div" id="quote">
		<div class="form-header" style="">
		<div style="position:relative; float:right; margin-bottom:10px;"><button id="btn-form" data-target="quote" class="btn btn-close " type="button">X</button></div>
		<div class="form-title">Get Your Free Quote</div>
		</div>
		<div class="form-copy">
		Please fill out the form below. A representative will contact you to discuss your project.
		</div>
		<div style=" padding:0 8px;">
		
		<?php echo do_shortcode('[contact-form-7 id="56" title="Home Page Free Quote"]'); ?>
		</div>
</div>

<div class="form-div" id="demo">
		<div class="form-header" style="">
		<div style="position:relative; float:right; margin-bottom:10px;"><button id="btn-form" data-target="demo" class="btn btn-close " type="button">X</button></div>
		<div class="form-title">Request a Demo</div>
		</div>
		<div class="form-copy">
		Please fill out the form below. A representative will contact you to schedule your demo.
		</div>
		<div style=" padding:0 8px;">
		
		<?php echo do_shortcode('[contact-form-7 id="5" title="Request Demo"]'); ?>
		</div>
</div>
