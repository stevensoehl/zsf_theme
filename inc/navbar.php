<style>
.navbar-default {
    background-color: #000!important;
    border-color: #000!important;
}

.navbar-default .navbar-nav > li > a  {
    color: #fff!important;
    background-color: transparent;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    color: #fff!important;
    background-color: transparent;
}
.logo {padding:8px 0; }
@media (min-width: 768px) {
.navbar-collapse {
margin-top:15px;
}

.nav-link {

  color:#fff;
}

.navbar-right {
  margin-top:15px;
}

}

@media (max-width: 767px) {
.logo {padding-left: 8px;}

.nav-link {

  color:#fff;
}

}


</style>

<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a  href="<?php echo home_url(); ?>" title="CORE Muscle Fitness" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Logo'});"><img class="logo" src="<?php echo get_template_directory_uri();?>/img/core.png" alt="Core Muscle Fitness - the latest information on fitness, fitness products, supplements, diet and nutrition, and the best fitness programs for you to meet your fitness goals"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="<?php echo home_url(); ?>" title="CORE Muscle Fitness" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Home'});">Home</a></li>
            <li><a href="<?php echo home_url(); ?>/category/fitness-articles/" title="Fitness news" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Fitness Articles'});">Articles</a></li>
            <li><a href="<?php echo home_url(); ?>/category/fitness-videos/" title="watch informative videos related to fitness, diet, nutrition, and more" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Fitness Videos'});">Videos</a></li>
            <li><a href="<?php echo home_url(); ?>/fitness-products/" title="fitness supplements, equipment and attire at the best prices" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Fitness Products'});">Products</a></li>
            <li><a href="<?php echo home_url(); ?>/fitness-photo-gallery/" title="check out inspirational fitness photos" onclick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'TopNav', 'event_action': 'click', 'event_label': 'Fitness Photos'});">Photos</a></li>
            <!--<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>-->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php if(is_user_logged_in()){ ?>

<a class="nav-link" href="<?php echo wp_logout_url(); ?>" title="Logout"><i class="fa fa-sign-out"></i>
&nbsp;Logout</a>
   <?php } else { ?>
   <li class="dropdown" >
    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-sign-in"></i>
&nbsp;Login <span class="caret"></span></a>
       <ul class="dropdown-menu">
      <li style="padding:12px; background-color:#f7f7f7;"><?php get_template_part('inc/loginform'); ?></li>
</ul>
</li>
<?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
