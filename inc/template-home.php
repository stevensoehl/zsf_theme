<style>
@media (max-width: 767px) {
.right {
  margin-top: 25px;
}
}
</style>
<div class="row">

  <div class="col-xs-12">

    <h2 class="home-title">Fitness Articles</h2>

  </div>

</div>





<div class="row">

   <div class="col-xs-12 col-md-6">

   <?php query_posts('category_name=fitness-articles&showposts=1' );?>

   <?php if (have_posts()) : while (have_posts()) : the_post();

   $category = get_the_category();

   ?>

              <div class="home-image" ><a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Articles Main Image - <?php the_title(); ?>');">

			  <?php

                                        if ( has_post_thumbnail() ) {

                                                the_post_thumbnail('home-image');

                                            }?>

                                            </a> </div>

									<h3 class="h3 home-post-title"><a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>" onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Articles Main Title- <?php the_title(); ?>');"><?php the_title(); ?></a></h3>

                                   <div class="home-post-meta"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo $category[0]->cat_name; ?></div>

                                      <div class="home-post-excerpt"><?php the_excerpt()?></div>



   <?php endwhile; endif; ?>



    <?php wp_reset_query(); ?>



   </div>

     <div class="col-xs-12 col-md-6 right">

         <?php query_posts('category_name=fitness-articles&offset=1&showposts=4' );?>

   <?php if (have_posts()) : while (have_posts()) : the_post();

   $category = get_the_category();

   ?>

              <div class="row gap" style="padding:6px 0;border-bottom:1px solid #f7f7f7;">

                            <div class="col-xs-5">
                              <a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                                onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Articles Main Image - <?php the_title(); ?>');">

                        <?php

                                                        if ( has_post_thumbnail() ) {

                                                                the_post_thumbnail('home-image');

                                                            }?>

                                                            </a>
                            </div>

                                             <div class="col-xs-7">

									<div class="" style="font-size:16px;margin-bottom:7px;"><a style="color:#000;" href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                    onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Articles - <?php the_title(); ?>');"><?php the_title(); ?></a></div>

                                   <div style="font-size:13px;color:#999;"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo $category[0]->cat_name; ?></div>



                                      </div>

                                      </div>



   <?php endwhile; endif; ?>



    <?php wp_reset_query(); ?>


   </div>

</div>





<div class="row">

  <div class="col-xs-12">

    <h2 class="home-title" style="margin-top:50px;">Fitness Videos</h2>

  </div>

</div>





<div class="row">

   <div class="col-xs-12 col-md-6">

   <?php query_posts('category_name=fitness-videos&showposts=1' );?>

   <?php if (have_posts()) : while (have_posts()) : the_post();
  $video = get_post_meta($post->ID, "video", true);
  $vimeo = get_post_meta($post-ID, "vimeo",true);

   $category = get_the_category();

   ?>

   <?php
      if ( has_post_thumbnail() ) {

       the_post_thumbnail('medium');

                                  } else { ?>
	   <a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
       onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Videos Main Image - <?php the_title(); ?>');"><img src="https://img.youtube.com/vi/<?php echo $video; ?>/0.jpg" alt=""></a>
         <?php } ?>
									<h3 class="h3 home-post-title"><a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                    onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Videos Main Title - <?php the_title(); ?>');"><?php the_title(); ?></a></h3>

                                   <div class="home-post-meta"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo $category[0]->cat_name; ?></div>

                                      <div class="home-post-excerpt"><?php the_excerpt()?></div>



   <?php endwhile; endif; ?>



    <?php wp_reset_query(); ?>



   </div>

     <div class="col-xs-12 col-md-6 right">

         <?php query_posts('category_name=fitness-videos&offset=1&showposts=4' );?>

   <?php if (have_posts()) : while (have_posts()) : the_post();
    $vid_img = get_post_meta($post->ID, "image", true);
   $category = get_the_category();

   ?>

   <div class="row gap" style="padding:6px 0;border-bottom:1px solid #f7f7f7;">

                 <div class="col-xs-5">
                   <?php
                      if ( has_post_thumbnail() ) {

                       the_post_thumbnail('medium');

                                                  } else { ?>
                     <a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                       onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Videos Main Image - <?php the_title(); ?>');"><img src="<?php echo $vid_img; ?>" alt=""></a>
                         <?php } ?>

</div>
                                             <div class="col-xs-7">

                                               <div class="" style="font-size:16px;margin-bottom:7px;"><a style="color:#000;" href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>"
                                                 onClick="ga('send', 'event', 'Home Page', 'Click', 'Fitness Articles - <?php the_title(); ?>');"><?php the_title(); ?></a></div>

                                                                <div style="font-size:13px;color:#999;"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo $category[0]->cat_name; ?></div>

                                     </div>

                                      </div>





   <?php endwhile; endif; ?>



    <?php wp_reset_query(); ?>

   </div>

</div>
