<?php
 $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

  $query_args = array(
    'post_type' => 'post',
  'category_name' => 'fitness-videos',
    'posts_per_page' => 10,
    'paged' => $paged
  );
  // create a new instance of WP_Query
  $the_query = new WP_Query( $query_args );
?>

<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop


$video = get_post_meta($id, 'video' , true);
?>

<div class="row blog-post" style="padding:25px 0; border-bottom:1px solid #ccc; ">
  <div class="col-xs-5">
   <?php
      if ( has_post_thumbnail() ) {

       the_post_thumbnail('medium');

                                  } else { ?> 
         <img src="https://img.youtube.com/vi/<?php echo $video; ?>/0.jpg" alt="">
         <?php } ?>                                                      
  </div>
  
  <div class="col-xs-7">
      <div class="blog-post-title"><a style="color:#444;" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></div>
      <div class="col-xs-12 post-roll-meta" style="margin-bottom: 20px;">
 <?php $author_name = get_author_name( $user_id ); 
 $category = get_the_category(); ?>
<span class="author"  style="color:#58595b;">By:&nbsp;<?php echo $author_name; ?></span><span class="updated published" style="color:#58595b;">&nbsp;&middot;&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;&middot;&nbsp;&nbsp;in:&nbsp;<?php echo $category[0]->cat_name; ?>&nbsp;&nbsp;&middot;&nbsp;&nbsp;<a style="color:#58595b;" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Read More</a></span>
</div>
      <div class="blog-post-excerpt" style="padding-top: 20px;"><?php the_excerpt();?></div>
  </div>



</div>



<?php endwhile; ?>
<!-- pagination here -->
  <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
  <nav class="prev-next-posts">
    <div class="prev-posts-link" style="position: relative; float: left; font-size: 16px;">
      <?php echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); // display older posts link ?>
    </div>
    <div class="next-posts-link" style="position: relative; float: right; font-size: 16px;">
      <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
    </div>
  </nav>
<?php } ?>

<?php endif;
wp_reset_query(); ?>

<?php  //pagination(); ?>