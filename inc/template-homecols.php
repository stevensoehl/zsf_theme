<style>
	.big-home {width:100%;
	    -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover; margin-bottom:0; height:50vh;}
	[data-crm]{background-image: url(<?php echo get_template_directory_uri(); ?>/img/fitness-articles2.jpg); background-repeat:no-repeat; background-position:center center;}
	[data-website]{background-image: url(<?php echo get_template_directory_uri(); ?>/img/fitness-videos.jpg); background-repeat:no-repeat; background-position:center center;}
	[data-adserver] {background-image: url(<?php echo get_template_directory_uri(); ?>/img/Build-Your-Biceps.jpg); background-repeat:no-repeat; background-position:center center;}

	[data-pics] {background-image: url(<?php echo get_template_directory_uri(); ?>/img/fitness-pics.jpg); background-repeat:no-repeat; background-position:center center;}


	@media (min-width: 768px) {
   .pic-pad {padding:1px; }
	 .big-home-title {color:#fff;  padding-top:50%; padding-left:15px; padding-right:15px; font-size:30px;margin-bottom:0;    }
	 .big-home-copy {color:#fff;  margin-top:3px; padding-left:15px; padding-right:15px; font-size:18px; line-height:1.4;    }
	}



	@media (max-width: 767px) {
.pic-pad {padding: 1px; }
.big-home-title {color:#fff;  padding-top:40%; padding-left:15px; padding-right:15px; font-size:24px;margin-bottom:0;    }
.big-home-copy {color:#fff;  margin-top:3px; padding-left:15px; padding-right:15px; font-size:15px; line-height:1.4;    }
	}


	@media screen and (min-width:1500px){

			.big-home-title {color:#fff;  padding-top:70%; padding-left:15px; padding-right:15px; font-size:30px;margin-bottom:0;    }
			.big-home-copy {color:#fff;  margin-top:3px; padding-left:15px; padding-right:15px; font-size:18px; line-height:1.4;    }


	}
</style>



<section>
	<div class="col-xs-6 col-sm-6 col-md-3 pic-pad " style="">
		<div class="big-home" data-crm="crm">
			<div class="big-home-title"><a href="<?php echo home_url(); ?>/category/fitness-articles/" title="The latest fitness articles"
				style="color:#fff;" onClick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'HomeCol', 'event_action': 'click', 'event_label': 'Fitness Articles'});">Fitness Articles</a></div>
			<div class="big-home-copy">Stay informed on the all things fitness from nutrition to weight training tips.</div>



		</div>

	</div><!-- end col 1-->
<div class="col-xs-6 col-sm-6 col-md-3 pic-pad " style="">
		<div class="big-home" data-website="website">
			<div class="big-home-title"><a href="<?php echo home_url(); ?>/category/fitness-videos/" title="The latest fitness articles"
				style="color:#fff;" onClick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'HomeCol', 'event_action': 'click', 'event_label': 'Fitness Videos'});">Fitness Videos</a></div>
			<div class="big-home-copy">Watch and learn from the pros showcase their weight training programs.</div>

		</div>

	</div><!-- end col 2-->
	<div class="col-xs-6 col-sm-6 col-md-3 pic-pad " style="">
		<div class="big-home" data-adserver="adserver">
			<div class="big-home-title"><a href="<?php echo home_url(); ?>/fitness-products/" title="fitness supplements, equipment and attire at the best prices"
				style="color:#fff;" onClick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'HomeCol', 'event_action': 'click', 'event_label': 'Fitness Products'});">Fitness Supplements</a></div>
			<div class="big-home-copy">Get the best prices on today's proven fitness supplements to enhance your gains.</div>

		</div>

	</div><!-- end col 3-->
	<div class="col-xs-6 col-sm-6 col-md-3 pic-pad " style="">
		<div class="big-home" data-pics="pics">
				<div class="big-home-title"> <a href="<?php echo home_url(); ?>/fitness-photo-gallery/" title="The latest fitness pics"
					style="color:#fff;" onClick="gtag('event', 'click', { 'send_to': 'UA-145246340-1', 'event_category': 'HomeCol', 'event_action': 'click', 'event_label': 'Fitness Photos'});">Fitness Pics</a></div>
			<div class="big-home-copy">Check out fitness photos that will inspire you to reach your goals</div>
		</div>

	</div><!-- end col 4-->


</section>
