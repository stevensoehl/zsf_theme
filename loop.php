<style>
@media (max-width: 767px) {
.blog-post-title {
  margin: 10px 0;
	font-size:18px;
	line-height:1.3;
	text-align:center;
}
}
</style>
<?php if (have_posts()): while (have_posts()) : the_post();
$video = get_post_meta($post->ID, 'video' , true);
?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="row blog-post" style="padding:25px 0; border-bottom:1px solid #ccc; ">
		  <div class="col-xs-12 col-sm-6 col-md-5">
		   <?php
		      if ( has_post_thumbnail() ) {
		       the_post_thumbnail();
				 } else if (!empty($video)){ ?>
					 	<img src="https://img.youtube.com/vi/<?php echo $video; ?>/0.jpg" alt="">
				 <?php  } else {}

						?>

		  </div>

		  <div class="col-xs-12 col-sm-6 col-md-7">
		  <div class="blog-post-title"><a style="color:#444;" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></div>
		      <div class="col-xs-12 post-roll-meta" style="margin-bottom: 20px;">
		 <?php $author_name = get_author_name( $user_id );
		 $category = get_the_category(); ?>
		<span class="author"  style="color:#58595b;">By:&nbsp;<?php echo $author_name; ?></span><span class="updated published" style="color:#58595b;">&nbsp;&middot;&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_date('F d, Y'); ?>&nbsp;&nbsp;&middot;&nbsp;&nbsp;in:&nbsp;<?php echo $category[0]->cat_name; ?>&nbsp;&nbsp;&middot;&nbsp;&nbsp;<a style="color:#58595b;" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Read More</a></span>
		</div>
		      <div class="blog-post-excerpt" style="padding-top: 20px;"><?php the_excerpt();?></div>

			</div>


		</div>



	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
		<?php edit_post_link(); ?>
