<?php
//include __DIR__ . '/m/vendor/autoload.php';
//use Mautic\Auth\ApiAuth;
//use Mautic\MauticApi;
//session_start();

date_default_timezone_set('America/New_York');
$year = date("Y");
$path = '//www.zachsoehlfitness.com/';


add_action( 'phpmailer_init', 'setup' );
function setup( PHPMailer $phpmailer ) {
    $phpmailer->Host = 'mailhog';
    $phpmailer->Port = 1025;
    $phpmailer->IsSMTP();
}

add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}

function create_user_from_registration($cfdata) {
    if (!isset($cfdata->posted_data) && class_exists('WPCF7_Submission')) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();
        if ($submission) {
            $formdata = $submission->get_posted_data();
        }
    } elseif (isset($cfdata->posted_data)) {
        // For pre-3.9 versions of Contact Form 7
        $formdata = $cfdata->posted_data;
    } else {
        // We can't retrieve the form data
        return $cfdata;
    }
    // Check this is the user registration form
    if ( $cfdata->title() == 'Newsletter') {
        $password = wp_generate_password( 12, false );
        $email = $formdata['email'];
        //$name = $formdata['fname'];
        // Construct a username from the user's name
        //$username = strtolower(str_replace(' ', '', $name));
      //  $name_parts = explode(' ',$name);
        //$fname = reset($name_parts);
        //$lname = end($name_parts);
        if ( !email_exists( $email ) ) {
            // Find an unused username
          //  $username_tocheck = $username;
          //  $i = 1;
          //  while ( username_exists( $username_tocheck ) ) {
            //    $username_tocheck = $username . $i++;
        //    }
          //  $username = $username_tocheck;
            // Create the user
            $userdata = array(
                'user_login' => $email,
                'user_pass' => $password,
                'user_email' => $email,
              //  'nickname' => reset($name_parts),
              //  'display_name' => $name,
              //  'first_name' => reset($name_parts),
              //  'last_name' => end($name_parts),
                'role' => 'member'
            );
            $user_id = wp_insert_user( $userdata );



            //if ( !is_wp_error($user_id) ) {
                // Email login details to user
                $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
                $message = "Welcome! Your login details are as follows:" . "\r\n";
                $message .= sprintf(__('Login: %s'), $email) . "\r\n";
                $message .= sprintf(__('Password: %s'), $password) . "\r\n";
                $message .= wp_login_url() . "\r\n";
				$message .= "Please login to complete your registration. You'll begin receiving valuable fitness tips that will help you meet your fitness goals"  . "\r\n";
				$headers = "From: webmaster@coremusclefitness.com" . "\r\n";
                mail($email, sprintf(__('[%s] Your username and password'), $blogname), $message, $headers);
            //}
			   $url = home_url('/almost-done/');
				wp_redirect( $url );
				exit;
        }
    }

    return $cfdata;
}
add_action('wpcf7_before_send_mail', 'create_user_from_registration', 1);


function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/login/login_styles.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');


    add_filter('login_redirect', 'user_login_redirect', 10, 3);
    function user_login_redirect() {
        $current_user = wp_get_current_user();
        $user = get_userdata($current_user->ID);
        $role = implode(', ', $user->roles);
        if ($role == 'member') {
          $location = home_url() . '/my-dashboard';
        } else if ($role == 'administrator') {
          $location = home_url() . '/wp-admin';
        } else {
        $location = $_SERVER['HTTP_REFERER'];
      }
      return $location;
    }


?>
