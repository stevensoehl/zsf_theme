<?php get_header(); ?>
<div class="container-fluid">
	<div class="row">
	<div class="col-xs-12 col-sm-9">
	<main role="main">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1><?php _e( 'Help Me! I\'m Lost', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Send me home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>
</div>
<div class="col-xs-12 col-sm-3">
<?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>
