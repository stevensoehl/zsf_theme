<?php get_header(); ?>
<?php get_template_part('inc/strip'); ?>
<div class="container-fluid">
	<div class="row">
	<div class="col-xs-12 col-sm-9">
	<main role="main">
		<!-- section -->
		<section>
			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>
</div>
<div class="col-xs-12 col-sm-3">
<?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>
