<?php get_header(); ?>
<div style="width:100%;  background-color:#e8e8e8;padding:12px;">

	<div class="container">
	<h1 class="the-title">
	<?php // _e( 'Archives', 'html5blank' ); ?>
	</h1></div>
</div>
<div class="container-fluid">
	<div class="row">
	<div class="col-xs-12 col-sm-9">
	<main role="main">
		<!-- section -->
		<section>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>
</div>
<div class="col-xs-12 col-sm-3">
<?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>
