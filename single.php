<?php get_header(); ?>
<?php get_template_part('inc/strip'); ?>
<div class="container-fluid" style="margin-top: 35px;">
	<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-9">
	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post();
		$video = get_post_meta($post->ID, 'video', true);
		$sups = get_post_meta($post->ID, 'sups', true);
		?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				</a>
			<?php endif; ?>
			<div class="clearfix" style="margin-top:9px;"></div>
			<!-- /post thumbnail -->

			<!-- post title -->

			<!-- /post title -->

			<!-- post details -->
			<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
			<span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
			<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
			<div class="clearfix"></div>
			<!-- /post details -->
			<div style="font-size: 16px; margin-top: 17px; margin-bottom: 25px;line-height:1.6;">
			<?php the_content(); // Dynamic Content ?>
			</div>
			<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

			<p><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			<p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>
<div class="clearfix"></div>
     <?php if ($sups == true) { ?>
			<div style="margin-top:25px;">
				<div class="woocommerce columns-4">

									<div class="products">

			 <?php
				$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => $sups , 'product_tag' => 'featured' );

												 $loop = new WP_Query( $args );

												 while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


					<div class="col-xs-6 col-md-3 product-cats" style="padding:4px; margin-top:10px; margin-bottom:10px; border:1px solid #ccc;">

																		 <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
																			 title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"
																			 onClick="ga('send', 'event', 'Product Page', 'Click', 'Test Booster Image - <?php the_title(); ?>');">

																				 <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

																				 <div class="top-grid-img">

																					 <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(180, 180)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />'; ?>

																				 </div>
																			 </a>
																				 <div class="" style="margin-top:15px; text-align:center;">

																					 <h3 style="font-size:14px; font-weight:600;"><?php the_title_limit(25, '…'); ?></h3>

																					 <span class="price" style="font-weight:600; color:#C90003;"><?php echo $product->get_price_html(); ?></span>
												<div style="margin-top:10px; padding-bottom:10px;">
													<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="btn btn-danger"
														 onClick="ga('send', 'event', 'Product Page', 'Click', 'Test Booster BUY NOW - <?php the_title(); ?>');">BUY NOW</a>
												</div>

																				 </div>



																 </div>

			 <?php
				 endwhile; ?>

										 <?php wp_reset_query(); ?>
			 </div>
			 </div>

			</div>
			<?php } ?>
			<!-- end products -->
<div class="clearfix"></div>
			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

			<?php comments_template(); ?>

		</article>
		<!-- /article -->

		<script>
var tag = document.createElement('script');

tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var video = '<?php echo $video; ?>'
    video.h = '390' //video iframe height
    video.w = '640' //video iframe width
var vidtitle = entry.title.$t;

function onYouTubeIframeAPIReady() {
player = new YT.Player('player', {
        height: video.h,
width: video.w,
videoId: video,
playerVars: { rel: 0, showinfo:0, autohide:1},
        events: {
            'onStateChange': onPlayerStateChange,
            'onError': onPlayerError
        }
     });
};

function onPlayerStateChange(event) {
    switch (event.data) {
        case YT.PlayerState.PLAYING:
            if (cleanTime() == 0) {
                //console.log('started ' + cleanTime());
                ga('send', 'event', 'Video', 'started', '<?php the_title(); ?>');
            } else {
                //console.log('playing ' + cleanTime())
                ga('send', 'event', 'Video', 'played', 'v: <?php the_title(); ?> | t: ' + cleanTime());
            };
            break;
        case YT.PlayerState.PAUSED:
            if (player.getDuration() - player.getCurrentTime() != 0) {
                //console.log('paused' + ' @ ' + cleanTime());
                ga('send', 'event', 'Video', 'paused', 'v: <?php the_title(); ?>  | t: ' + cleanTime());
            };
            break;
        case YT.PlayerState.ENDED:
            //console.log('ended ');
            ga('send', 'event', 'Video', 'ended', '<?php the_title(); ?>');

			break;
    };

	if(event.data === 0) {
				jQuery(document).ready(function(){

		    $('#demoModal').modal('show');

			});
			            }
};
//utility
function cleanTime(){
    return Math.round(player.getCurrentTime())
};

function onPlayerError (event) {
    switch(event.data) {
        case 2:
            //console.log('' + video.id)
            ga('send', 'event', 'Video', 'invalid id',video);
            break;
        case 100:
            ga('send', 'event', 'Video', 'not found',video);
            break;
        case 101 || 150:
            ga('send', 'event', 'Video', 'not allowed',video);
            break;
        };
};

</script>

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>
		</div>
<div class="col-xs-12 col-sm-4 col-md-3">
<?php get_sidebar();

?>
	</div>
</div>



<?php get_footer(); ?>
